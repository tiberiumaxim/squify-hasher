var crypto = require('crypto');
var base32 = require('base32');

var currentMirror = 0;


exports.encryptRc4 = function(text, secret) {
    secret      = secret || (process.env.secret);
    var cipher  = crypto.createCipher('rc4', secret); //was aes-256-cbc
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted     = crypted + cipher.final('hex');

    return crypted;
};

exports.decryptRc4 = function (text, secret) {

    secret       = secret || (process.env.secret);
    var decipher = crypto.createDecipher('rc4', secret);
    var dec      = decipher.update(text, 'hex', 'utf8');
    dec          = dec + decipher.final('utf8');

    return dec;
};

exports.decryptRc4Url = function (url, secret) {
    var quality      = url.slice(-6, -4);
    url              = url.slice(0, -7);
    var parts        = url.split('/');
    decryptedUrl     = exports.decryptRc4(parts.slice(5).join(''), secret);

    return {
        url: decryptedUrl,
        quality: quality
    };
};

exports.decodeBase32Url = function (url) {
    url          = url.slice(0, -7);
    var parts    = url.split('/');

    return base32.decode(parts.slice(5).join(''));
};

exports.createRelativeChromeshotUrl = function (url, quality, method, chromeshotSecret) {
    var transformed;
    if (method == 'rc4') {
        transformed = exports.encryptRc4(url, chromeshotSecret);
    }
    else {
        transformed = base32.encode(url);
    }

    var len = transformed.length;
    quality = quality || 'hq';

    var prefix  = transformed[len - 1] + '/' +
                  transformed[len - 2] + '/' +
                  transformed[len - 3] + '/' +
                  transformed[len - 4] + '/' +
                  transformed[len - 5] + '/';

    var suffix       = transformed.match(/.{1,240}/g).join('/');
    var relativePath = prefix + suffix + '_' + quality + '.png';

    return relativePath;
};

exports.createFullChromeshotUrl = function (url, quality, chromeshotSecret, chromeshotUrl, maxMirrorCount) {
    if (++currentMirror > maxMirrorCount) {
        currentMirror = 1;
    }

    var finalUrl = chromeshotUrl.replace('{#}', currentMirror) + exports.createRelativeChromeshotUrl(url, quality, 'rc4', chromeshotSecret);

    return finalUrl;
};